/** @jsx React.DOM */
'use strict';
var HelloWorld = React.createClass({
	getInitialState: function() {
		return {message: 'Hello World!'};
	},
	goodbye: function(event) {
		this.setState({message: 'Goodbye World.'});
	},
	render: function() {
		return (
			<h1 onClick={this.goodbye}>{this.state.message}</h1>
		);
	}
});

var testModel = {
  "period" : "May 2014",
  "items"  : [
    { 
      "description" : "Rent",
      "type"        : "expense",
      "budget"      : 1600,
      "actual"      : 1600
    },
    {
      "description" : "Salary",
      "type"        : "income",
      "budget"      : 5300,
      "actual"      : 4875
    },
    { 
      "description" : "Groceries",
      "type"        : "expense",
      "budget"      : 800,
      "actual"      : 650
    }
  ],
  "budgetExpenses"  : 2400,
  "budgetIncome"    : 5300,
  "budgetRemainder" : 2900,
  "actualExpenses"  : 2250,
  "actualIncome"    : 4875,
  "actualRemainder" : 2625
};

var rent = { description : "Rent",
type : "expense",
budget : 1600,
actual : 1600
}

var Item = React.createClass({
  render: function() {
  return <div className="row">
      <div className="small-2 large-3 columns">
      <span>{ this.props.description }</span>
      </div>
      <div className="small-5 large-4 columns">
      <input type="text" placeholder="budget" value={ this.props.budget } />
      </div>
      <div className="small-5 large-4 columns">
      <input  type="text" placeholder="actual" value={ this.props.actual } />
      </div>
    </div>;
  }
});


React.renderComponent(
  <Item description={rent.description} budget={rent.budget} actual={rent.actual} />,
  document.getElementById('app')
);